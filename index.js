
// Bai 1
const kvA = "a";
const kvB = "b";
const kvC = "c";
const object1 = "1";
const object2 = "2";
const object3 = "3";



function chonDoiTuong(dt) {
    switch (dt) {
        case object1:
            return 2.5;
        case object2:
            return 1.5;
        case object3:
            return 1;
        default:
            return 0;
    }
}


function chonKhuVuc(kv) {
    switch (kv) {
        case kvA:
            return 2;
        case kvB:
            return 1;
        case kvC:
            return 0.5;
        default:
            return 0;
    }
}


function tinhDiemBaMon(a, b, c) {
    var sum = a + b + c;
    // console.log('sum: ', sum);
    return sum;
}


function tinhKetQua() {
    // console.log("yes");
    var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
    var khuVuc = document.getElementById("chon-khu-vuc").value;
    var doiTuong = document.getElementById("chon-doi-tuong").value;
    var diemThuNhat = document.getElementById("txt-diem-thu-nhat").value * 1;
    var diemThuHai = document.getElementById("txt-diem-thu-hai").value * 1;
    var diemThuBa = document.getElementById("txt-diem-thu-ba").value * 1;

    var sumBaMon = tinhDiemBaMon(diemThuNhat, diemThuHai, diemThuBa);
    // console.log('sumBaMon: ', sumBaMon);
    var chooseRegion = chonKhuVuc(khuVuc);
    // console.log('chooseRegion: ', chooseRegion);
    var chooseObject = chonDoiTuong(doiTuong);
    // console.log('chooseObject: ', chooseObject);

    var tongDiem = 0;

    if (diemThuNhat == 0 || diemThuHai == 0 || diemThuBa == 0) {
        document.getElementById("resultBai1").innerHTML = `Bạn đã rớt do có điểm nhỏ hơn hoặc bằng không`;
        return;
    }

    tongDiem = sumBaMon + chooseRegion + chooseObject;
    if (tongDiem >= diemChuan) {
        document.getElementById("resultBai1").innerHTML = `Bạn đã đậu. Tổng điểm là : ${tongDiem}`;
    } else {
        document.getElementById("resultBai1").innerHTML = `Bạn đã rớt. Tổng điểm là : ${tongDiem}`;
    }
}



// Bai 2


function tinhTien50KwDau(kw) {
    var giaTien = 0;
    giaTien = kw * 500;
    return giaTien;
}

function tinhTien50KwTiep(kw) {
    var giaTien = 0;
    giaTien = 50 * 500 + (kw - 50) * 650;
    return giaTien;
}

function tinhTien100KwTiep(kw) {
    var giaTien = 0;
    giaTien = 50 * 500 + 50 * 650 + (kw - 100) * 850;
    return giaTien;
}

function tinhTien150KwTiep(kw) {
    var giaTien = 0;
    giaTien = 50 * 500 + 50 * 650 + 100 * 850 + (kw - 200)*1100;
    return giaTien;
}

function tinhTienKwConLai(kw){
    var giaTien = 0;
    giaTien = 50 * 500 + 50 * 650 + 100 * 850 + 150*1100 + (kw - 350)*1300;
    return giaTien;
}


function tinhTienDien() {
    // console.log("yes");
    var hoTen = document.getElementById("txt-ho-ten").value;
    var soKw = document.getElementById("txt-so-kw").value;

    var tien50KwDau = tinhTien50KwDau(soKw);
    // console.log('tien50KwDau: ', tien50KwDau.toLocaleString());
    var tien50KwTiep = tinhTien50KwTiep(soKw);
    // console.log('tien50KwTiep: ', tien50KwTiep.toLocaleString());
    var tien100KwTiep = tinhTien100KwTiep(soKw);
    // console.log('tien100KwTiep: ', tien100KwTiep.toLocaleString());
    var tien150KwTiep = tinhTien150KwTiep(soKw);
    // console.log('tien150KwTiep: ', tien150KwTiep.toLocaleString());
    var tienKwConLai = tinhTienKwConLai(soKw);
    // console.log('tienKwConLai: ', tienKwConLai.toLocaleString());

    if(soKw <= 50){
        document.getElementById("resultBai2").innerHTML = `
        Họ tên : ${hoTen} ; Tiền điện : ${tien50KwDau.toLocaleString()}
        `;
    }else if(soKw <= 100){
        document.getElementById("resultBai2").innerHTML = `
        Họ tên : ${hoTen} ; Tiền điện : ${tien50KwTiep.toLocaleString()}
        `;
    }else if(soKw <= 200){
        document.getElementById("resultBai2").innerHTML = `
        Họ tên : ${hoTen} ; Tiền điện : ${tien100KwTiep.toLocaleString()}
        `;
    }else if(soKw <= 350){
        document.getElementById("resultBai2").innerHTML = `
        Họ tên : ${hoTen} ; Tiền điện : ${tien150KwTiep.toLocaleString()}
        `;
    }else{
        document.getElementById("resultBai2").innerHTML = `
        Họ tên : ${hoTen} ; Tiền điện : ${tienKwConLai.toLocaleString()}
        `;
    }
}